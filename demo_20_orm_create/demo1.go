package main

import (
	"database/sql"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type User struct {
	ID   int64
	Name sql.NullString `gorm:"default:'二狗'"` // 表列字段的默认值
	Age  int64
}

func main() {
	db, err := gorm.Open("mysql", "root:123456@(10.10.10.101:33306)/db1?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// 自动创建表  自动迁移 （把结构体和数据表进行映射）
	db.AutoMigrate(&User{})

	// 添加数据
	// 如果不设置Name属性，默认就是 二狗，设置空字符串""会被过滤，也是默认值
	// u := User{Name: "", Age: 18}
	// 如果想要设置不传Name属性时候保存就想要空值，就要给Name属性定义类型是 sql.NullString
	// u := User{Age: 18}
	// 如果要传入的Name 的值是""不是nil，就要这样传,告诉它，这个Name是有值的，是""
	u := User{Name: sql.NullString{String: "", Valid: true}, Age: 18}
	fmt.Println(db.NewRecord(&u)) // 判断主键是否为空	true
	db.Create(&u)
	fmt.Println(db.NewRecord(&u)) // false
}
