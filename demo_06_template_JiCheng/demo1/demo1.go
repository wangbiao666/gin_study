package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func index(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("demo_06_template_JiCheng/demo1/demo1_index.tmpl")
	if err != nil {
		fmt.Printf("parse template failed, err: %v\n", err)
		return
	}
	t.Execute(w, "这是index页面")
}
func home(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("demo_06_template_JiCheng/demo1/demo1_home.tmpl")
	if err != nil {
		fmt.Printf("parse template failed, err: %v\n", err)
		return
	}
	t.Execute(w, "这是home页面")
}

func main() {
	http.HandleFunc("/index", index)
	http.HandleFunc("/home", home)

	// 分别访问这两个路径，对应的两个模板内容的格式是一样的，只是返回的数据不一样
	// 所有这就很冗余，其实使用一个模板就可以了，看demo2的目录

	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Printf("http server failed, err: %v", err)
		return
	}
}
