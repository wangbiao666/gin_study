package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func index(w http.ResponseWriter, r *http.Request) {
	// 要解析几个模板，就填几个
	t, err := template.ParseFiles("demo_06_template_JiCheng/demo2/templates/base.tmpl", "demo_06_template_JiCheng/demo2/templates/index.tmpl")
	if err != nil {
		fmt.Printf("parse template failed, err: %v\n", err)
		return
	}
	// name 参数是模板名字，不要写路径
	err = t.ExecuteTemplate(w, "index.tmpl", "这是index页面")
}
func home(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("demo_06_template_JiCheng/demo2/templates/base.tmpl", "demo_06_template_JiCheng/demo2/templates/home.tmpl")
	if err != nil {
		fmt.Printf("parse template failed, err: %v\n", err)
		return
	}
	t.ExecuteTemplate(w, "home.tmpl", "这是home页面")
}

func main() {
	http.HandleFunc("/index", index)
	http.HandleFunc("/home", home)

	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Printf("http server failed, err: %v", err)
		return
	}
}
