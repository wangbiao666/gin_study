package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// 一般用于获取GET请求后缀的参数

func main() {
	r := gin.Default()

	r.GET("/web", func(context *gin.Context) {
		// 获取请求携带的参数   http://localhost:9090/web?query=王二狗
		//name := context.Query("query")
		// 如果有两个参数，道理一样		 http://localhost:9090/web?query=王二狗&age=18
		// age := context.Query("age")

		// 如果没有 query 这个参数属性，就用默认值		http://localhost:9090/web
		//name := context.DefaultQuery("query", "无参数，用默认值")

		// 如果没有 query 这个参数属性，就用默认值	 http://localhost:9090/web
		name, ok := context.GetQuery("query")
		if !ok {
			name = "无参数，用默认值"
		}

		context.JSON(http.StatusOK, gin.H{
			"name": name,
			//"age":  age,
		})
	})

	r.Run(":9090")
	// 测试：浏览器访问：

}
