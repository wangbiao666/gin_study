package main

import (
	"github.com/gin-gonic/gin"
	"html/template"
	"net/http"
)

func main() {
	r := gin.Default()
	r.SetFuncMap(template.FuncMap{
		"safe": func(str string) template.HTML {
			return template.HTML(str)
		},
	})
	r.LoadHTMLFiles("demo_08_gin_template/template/users/index.tmpl")

	// 设置 xxx 映射到了指定静态文件目录，模板中就可以直接使用这个 xxx 了
	r.Static("/xxx", "demo_08_gin_template/statics")

	r.GET("/users/index", func(context *gin.Context) {
		// 模板渲染  name参数：如果模板文件没有自定义名字，那就是这个模板的名字
		context.HTML(http.StatusOK, "users/index.tmpl", gin.H{
			"title": "<a href='https://www.baidu.com/'>www.baidu.com</a>",
		})
	})
	r.Run(":9090")
}
