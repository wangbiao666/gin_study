package main

import (
	"github.com/gin-gonic/gin"
	"html/template"
	"net/http"
)

func main() {
	r := gin.Default()

	// gin框架中自定义函数到模板中使用
	// 这个必须要写在解析模板之前！！！
	r.SetFuncMap(template.FuncMap{
		"safe": func(str string) template.HTML {
			return template.HTML(str)
		},
	})

	// 模板解析  如果有一百个文件呢？
	// r.LoadHTMLFiles("demo_08_gin_template/template/posts/index.tmpl", "demo_08_gin_template/template/users/index.tmpl")

	// 所以可以这样写  ** 表示目录  * 是文件
	r.LoadHTMLGlob("demo_08_gin_template/template/**/*")

	// 请求路径
	r.GET("/posts/index", func(context *gin.Context) {
		// 模板渲染    name属性名要和模板里定义的名一样
		context.HTML(http.StatusOK, "posts/index.tmpl", gin.H{
			"title": "aaaaaaaaaaa",
		})
	})
	r.GET("/users/index", func(context *gin.Context) {
		// 模板渲染
		context.HTML(http.StatusOK, "users/index.tmpl", gin.H{
			"title": "<a href='https://www.baidu.com/'>www.baidu.com</a>",
		})
	})
	r.Run(":9090")
}
