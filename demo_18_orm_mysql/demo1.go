package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// orm - 对象关系映射

// 尝试连接mysql：
// 先导入：
//		github.com/jinzhu/gorm
// 		github.com/jinzhu/gorm/dialects/mysql
// 数据库创建好db1 数据库

type UserInfo struct {
	ID     uint
	Name   string
	Gender string
	Hobby  string
}

func main() {
	db, err := gorm.Open("mysql", "root:123456@(10.10.10.101:33306)/db1?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// 自动创建表  自动迁移 （把结构体和数据表进行映射）
	db.AutoMigrate(&UserInfo{})

	// 创建数据行
	//u1 := UserInfo{1, "王二狗", "男", "翠花"}
	//db.Create(&u1)

	var u UserInfo
	// 查询
	//db.First(&u) // 查询第一条数据
	//fmt.Printf("%#v\n", u)

	// 更新
	//db.Model(&u).Update("hobby", "鞠婧祎")

	// 删除
	db.Delete(&u)
}
