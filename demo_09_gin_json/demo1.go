package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()

	r.GET("/json", func(context *gin.Context) {
		// 返回数据：
		// 方法1： map

		//data := map[string]interface{}{
		//	"name":    "王二狗",
		//	"message": "message",
		//	"age":     18,
		//}

		// 方法2： Gin框架提供的 gin.H

		//data := gin.H{
		//	"name":    "王二狗",
		//	"message": "message",
		//	"age":     18,
		//}

		// 方法3： 结构体
		// 结构体属性的首字母一定要大写，小写的话前端就收不到这个属性了！！！
		// 如果必须要指定小写字母，那就自己定义一下tag，如：Name属性
		type msg struct {
			Name    string `json:"name"`
			Message string
			Age     int
		}
		data := msg{
			Name:    "王二狗",
			Message: "message",
			Age:     18,
		}

		// 指定返回前端数据格式是 json
		context.JSON(http.StatusOK, data)
	})

	r.Run(":9090")
}
