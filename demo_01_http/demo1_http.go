package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

/*
	先来看下原生的go写法web请求
*/

// 这两个参数表示可以拿到请求和响应
func sayHello(w http.ResponseWriter, r *http.Request) {
	// 响应内容给前端
	//fmt.Fprintln(w,"<h1>hello golang\nhow ary you</h1>")

	// 把指定文件里的内容响应给前端
	// 先读取文件
	file, _ := ioutil.ReadFile("./demo1.txt")
	fmt.Fprintln(w, string(file))
}
func main() {
	// 请求到/demo1路径,就调用到syHello方法
	http.HandleFunc("/demo1", sayHello)

	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Printf("http server failed, err: %v", err)
		return
	}
}
