package main

import "github.com/gin-gonic/gin"

// restfull 风格的请求函数实例

func main() {
	r := gin.Default()

	// 比如要对图书增删改查
	r.GET("/book", func(context *gin.Context) {
		context.JSON(200, gin.H{
			"method": "GET",
		})
	})
	r.POST("/book", func(context *gin.Context) {
		context.JSON(200, gin.H{
			"method": "POST",
		})
	})
	r.PUT("/book", func(context *gin.Context) {
		context.JSON(200, gin.H{
			"method": "PUT",
		})
	})
	r.DELETE("/book", func(context *gin.Context) {
		context.JSON(200, gin.H{
			"method": "DELETE",
		})
	})

	// 使用postmain 分别以这4种请求方式请求 http://localhost:9090/book

	r.Run(":9090")
}
