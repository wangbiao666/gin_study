package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()

	r.GET("/index", func(context *gin.Context) {
		//context.JSON(http.StatusOK, gin.H{
		//	"status": "ok",
		//})

		// 重定向别的链接
		//context.Redirect(http.StatusMovedPermanently, "https://www.baidu.com/")

		// 重定向到其他路由
		context.Request.URL.Path = "/a" // 下面的路由
		r.HandleContext(context)
	})
	r.GET("/a", func(context *gin.Context) {
		context.JSON(http.StatusOK, gin.H{
			"status": "redirect",
		})
	})

	r.Run(":9090")
}
