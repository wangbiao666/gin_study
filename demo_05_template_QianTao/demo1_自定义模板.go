package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func f1(w http.ResponseWriter, r *http.Request) {
	// 自己创建的模板名字必须和使用名字一样
	t, err := template.New("demo1.tmpl").
		ParseFiles("demo_05_template_QianTao/demo1.tmpl")
	if err != nil {
		fmt.Printf("parse template failed, err: %v\n", err)
		return
	}
	t.Execute(w, "aaaaaaaaaaaaaaaa")

}
func main() {
	http.HandleFunc("/demo1", f1)

	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Printf("http server failed, err: %v\n", err)
		return
	}

}
