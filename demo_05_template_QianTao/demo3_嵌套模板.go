package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func f3(w http.ResponseWriter, r *http.Request) {
	// 参数1：父级模板   参数2： 子级要继承的模板
	t, err := template.ParseFiles("demo_05_template_QianTao/demo3.tmpl", "demo_05_template_QianTao/demo3_ul.tmpl")
	if err != nil {
		fmt.Printf("parse template failed, err: %v\n", err)
		return
	}
	t.Execute(w, "aaaaaaaaaaaaaaaa")

}
func main() {
	http.HandleFunc("/demo3", f3)

	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Printf("http server failed, err: %v\n", err)
		return
	}

}
