package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func f2(w http.ResponseWriter, r *http.Request) {
	// 模板中使用自定义函数
	kua := func(name string) (string, error) {
		return name + "年轻又帅气", nil
	}

	// 自己创建的模板名字必须和使用名字一样
	t := template.New("demo2_test3.tmpl")
	t.Funcs(template.FuncMap{
		"kuaaaa": kua,
	})
	_, err := t.ParseFiles("demo_05_template_QianTao/demo2_test3.tmpl")
	if err != nil {
		fmt.Printf("parse template failed, err: %v\n", err)
		return
	}

	t.Execute(w, "aaaaaaaaaaaaaaaa")

}
func main() {
	http.HandleFunc("/demo2", f2)

	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Printf("http server failed, err: %v\n", err)
		return
	}

}
