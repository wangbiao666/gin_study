package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// 从url 中 获取参数, 获取到的值都是string类型
// 注意多个url不要冲突

func main() {
	r := gin.Default()

	// 测试： 浏览器请求 http://localhost:9090/user/二狗/18
	r.GET("/user/:name/:age", func(context *gin.Context) {
		name := context.Param("name")
		age := context.Param("age")
		context.JSON(http.StatusOK, gin.H{
			"name": name,
			"age":  age,
		})
	})

	// 浏览器请求： http://localhost:9090/blog/2021/07
	r.GET("/blog/:year/:month", func(context *gin.Context) {
		year := context.Param("year")
		month := context.Param("month")
		context.JSON(http.StatusOK, gin.H{
			"year":  year,
			"month": month,
		})
	})

	r.Run(":9090")
}
