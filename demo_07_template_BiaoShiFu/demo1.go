package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func index(w http.ResponseWriter, r *http.Request) {
	t, err := template.New("index.tmpl").
		Delims("{[", "]}"). // 自定义的模板标识符
		ParseFiles("demo_07_template_BiaoShiFu/index.tmpl")
	if err != nil {
		fmt.Printf("parse template failed, err: %v\n", err)
		return
	}
	t.Execute(w, "aaaaaaaaaaaaaaa")
}
func xss(w http.ResponseWriter, r *http.Request) {
	// 自定义函数
	t, err := template.New("xss.tmpl").Funcs(template.FuncMap{
		"safe": func(str string) template.HTML {
			return template.HTML(str)
		},
	}).ParseFiles("demo_07_template_BiaoShiFu/xss.tmpl")
	if err != nil {
		fmt.Printf("parse template failed, err: %v\n", err)
		return
	}
	str1 := "<script>alert(123);</script>"
	str2 := "<a href='https://www.baidu.com/'>www.baidu.com</a>"
	t.Execute(w, map[string]string{
		"str1": str1,
		"str2": str2,
	})
}

func main() {
	http.HandleFunc("/index", index)
	http.HandleFunc("/xss", xss)

	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Printf("http server failed, err: %v", err)
		return
	}
}
