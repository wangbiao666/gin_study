package main

import "github.com/gin-gonic/gin"

// 初识 Gin, 要先去下载 go get -u github.com/gin-gonic/gin

// 参数必须是 *gin.Context
func hello(c *gin.Context) {
	// 直接就可以写返回的内容
	// 返回json,就直接 .JSON
	c.JSON(200, gin.H{ // gin.H 搞成json格式,其实是map,值是接口,所以可以是任意类型
		"message": "hello gin",
	})
}
func main() {
	// 返回默认的路由引擎
	r := gin.Default()
	r.GET("/demo2", hello)

	// 启动,默认不写的话是8080端口
	r.Run(":9090")
}
