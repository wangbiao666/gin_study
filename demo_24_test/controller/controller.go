package controller

import (
	"gin_demo/demo_24_test/models"
	"github.com/gin-gonic/gin"
	"net/http"
)

func IndexHandler(context *gin.Context) {
	context.HTML(http.StatusOK, "index.html", nil)
}

func CreateATodo(context *gin.Context) {
	var todo models.Todo
	context.BindJSON(&todo)
	err := models.CreateTodo(&todo)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{"error": err.Error()})
	} else {
		context.JSON(http.StatusOK, todo)
		// 有的公司会要求返回这种格式：
		//context.JSON(http.StatusOK, gin.H{
		//	"code": 200,
		//	"msg":  "success",
		//	"data": todo,
		//})
	}
}

func GetTodoList(context *gin.Context) {
	todoList, err := models.GetAllTodo()
	if err != nil {
		context.JSON(http.StatusOK, err.Error())
	} else {
		context.JSON(http.StatusOK, todoList)
	}
}
func UpdateTodo(context *gin.Context) {
	id, ok := context.Params.Get("id")
	if !ok {
		context.JSON(http.StatusOK, gin.H{"error": "无效的id"})
		return
	}
	todo, err := models.GetATodo(id)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{"error": err.Error()})
	}
	context.BindJSON(&todo)
	if err = models.UpdateaATodo(todo); err != nil {
		context.JSON(http.StatusOK, gin.H{"error": err.Error()})
	} else {
		context.JSON(http.StatusOK, todo)
	}
}
func DeleteTodo(context *gin.Context) {
	id, ok := context.Params.Get("id")
	if !ok {
		context.JSON(http.StatusOK, gin.H{"error": "无效的id"})
		return
	}
	if err := models.DeleteATodo(id); err != nil {
		context.JSON(http.StatusOK, gin.H{"error": err.Error()})
	} else {
		context.JSON(http.StatusOK, gin.H{id: "deleted"})
	}
}
