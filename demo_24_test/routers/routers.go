package routers

import (
	"gin_demo/demo_24_test/controller"
	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()
	r.Static("demo_24_test/static", "static")  // 告诉gin框架去哪找静态资源
	r.LoadHTMLGlob("demo_24_test/templates/*") // 告诉gin框架去哪找页面

	r.GET("/", controller.IndexHandler)

	v1Group := r.Group("v1")
	{
		// 添加
		v1Group.POST("/todo", controller.CreateATodo)
		// 查看
		v1Group.GET("/todo", controller.GetTodoList)
		// 修改
		v1Group.PUT("/todo/:id", controller.UpdateTodo)
		// 删除
		v1Group.DELETE("/todo/:id", controller.DeleteTodo)
	}
	return r
}
