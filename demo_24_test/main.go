package main

import (
	"gin_demo/demo_24_test/dao"
	"gin_demo/demo_24_test/models"
	"gin_demo/demo_24_test/routers"
)

func main() {
	// 连接数据库
	err := dao.InitMySQL()
	if err != nil {
		panic(err)
	}
	defer dao.DB.Close()               // 程序退出关闭数据库连接
	dao.DB.AutoMigrate(&models.Todo{}) // 创建表

	r := routers.SetupRouter()
	r.Run()
}
