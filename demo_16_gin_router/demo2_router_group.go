package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()

	// 如下：这三个路由的前缀都是video
	r.GET("/video/index", func(context *gin.Context) {
		context.JSON(http.StatusOK, gin.H{"msg": "/video/index"})
	})
	r.GET("/video/xx", func(context *gin.Context) {
		context.JSON(http.StatusOK, gin.H{"msg": "/video/xx"})
	})
	r.GET("/video/oo", func(context *gin.Context) {
		context.JSON(http.StatusOK, gin.H{"msg": "/video/oo"})
	})

	// 可以使用路由组，如：
	shopGroup := r.Group("/shop")
	{
		shopGroup.GET("/index", func(context *gin.Context) {
			context.JSON(http.StatusOK, gin.H{"msg": "/shop/index"})
		})
		shopGroup.GET("/xx", func(context *gin.Context) {
			context.JSON(http.StatusOK, gin.H{"msg": "/shop/xx"})
		})
		shopGroup.GET("/oo", func(context *gin.Context) {
			context.JSON(http.StatusOK, gin.H{"msg": "/shop/oo"})
		})
	}

	r.Run(":9090")
}
