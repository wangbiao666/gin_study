package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()

	// GET请求方式访问index路径时候，就会执行这个func函数
	// 常见的请求方式有GET、POST、PUT、DELETE
	r.GET("/index", func(context *gin.Context) {
		context.JSON(http.StatusOK, gin.H{
			"method": "GET",
		})
	})

	// 使用Any函数的路由可以处理所有请求方式
	r.Any("/user", func(context *gin.Context) {
		switch context.Request.Method {
		case "GET":
			context.JSON(http.StatusOK, gin.H{
				"method": "GET",
			})
		case "POST":
			context.JSON(http.StatusOK, gin.H{
				"method": "POST",
			})
		}
	})

	r.Run(":9090")
}
