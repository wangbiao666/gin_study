package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()

	// 当访问到不存在的路由，就自动跳转到这里然后可以做处理
	r.NoRoute(func(context *gin.Context) {
		context.JSON(http.StatusNotFound, gin.H{"msg": "请访问正确地址"})
	})

	r.Run(":9090")
}
