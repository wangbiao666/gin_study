package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()

	r.LoadHTMLFiles("demo_11_gin_form_param/login.html", "demo_11_gin_form_param/index.html")
	// 跳转到login.html
	r.GET("/login", func(context *gin.Context) {
		context.HTML(http.StatusOK, "login.html", nil)
	})
	// 接收login.html页面的form表单的请求
	r.POST("/login", func(context *gin.Context) {
		username := context.PostForm("username")
		password := context.PostForm("password")
		// 取参数值的时候，也可以根据场景使用函数，给默认值
		// DefaultPostForm 和 GetPostForm

		// 接收到form表单请求的参数，调整到index.html
		context.HTML(http.StatusOK, "index.html", gin.H{
			"Name":     username,
			"Password": password,
		})
	})

	r.Run(":9090")
}
