package main

import (
	"fmt"
	"html/template"
	"net/http"
)

type User struct {
	Name   string
	gender string // 首字母小写，页面中获取不到
	Age    int
}

func test1(w http.ResponseWriter, r *http.Request) {
	// 模板文件已经定义好, 第二步: 解析模板
	t, err := template.ParseFiles("demo_04_template/demo2_test1.tmpl")
	if err != nil {
		fmt.Printf("parse template failed, err: %v\n", err)
		return
	}
	// 第三步: 渲染模板，传入结构体
	u1 := User{
		Name:   "aBiu",
		gender: "男",
		Age:    18,
	}
	t.Execute(w, u1)
}

func test2(w http.ResponseWriter, r *http.Request) {
	// 模板文件已经定义好, 第二步: 解析模板
	t, err := template.ParseFiles("demo_04_template/demo2_test2.tmpl")
	if err != nil {
		fmt.Printf("parse template failed, err: %v\n", err)
		return
	}
	// 第三步: 渲染模板，传入map
	m1 := map[string]interface{}{
		"Name": "aBiu",
		// 首字母还是小写，map是可以渲染出来的
		"gender": "男",
		"Age":    18,
	}
	t.Execute(w, m1)
}

func test3(w http.ResponseWriter, r *http.Request) {
	// 模板文件已经定义好, 第二步: 解析模板
	t, err := template.ParseFiles("demo_04_template/demo2_test3.tmpl")
	if err != nil {
		fmt.Printf("parse template failed, err: %v\n", err)
		return
	}
	// 第三步: 渲染模板，传入map
	u1 := User{
		Name:   "aBiu",
		gender: "男",
		Age:    18,
	}
	m1 := map[string]interface{}{
		"name":   "aBiu",
		"gender": "男",
		"age":    18,
	}
	hobbyList := []string{
		"golang",
		"java",
		"python",
	}
	//isNilHobbyList := []string{}
	t.Execute(w, map[string]interface{}{
		"u1":    u1,
		"m1":    m1,
		"hobby": hobbyList,
		//"hobby": isNilHobbyList,		// 传入空的切片
	})
}

func main() {
	http.HandleFunc("/test1", test1)
	http.HandleFunc("/test2", test2)
	http.HandleFunc("/test3", test3)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Printf("http server start failed, err: %v\n", err)
		return
	}
}
