package main

import (
	"fmt"
	"html/template"
	"net/http"
)

/*
Go语言内置了文本模板引擎text/template和用于HTML文档的html/template。
它们的作用机制可以简单归纳如下：
	模板文件通常定义为.tmpl和.tpl为后缀（也可以使用其他的后缀），必须使用UTF8编码。
	模板文件中使用{{和}}包裹和标识需要传入的数据。
	传给模板这样的数据就可以通过点号（.）来访问，如果数据是复杂类型的数据，可以通过{ { .FieldName }}来访问它的字段。
	除{{和}}包裹的内容外，其他内容均不做修改原样输出。

模板引擎的使用:
	Go语言模板引擎的使用可以分为三部分：定义模板文件、解析模板文件和模板渲染.
*/

func demo1(w http.ResponseWriter, r *http.Request) {
	// 模板文件已经定义好, 第二步: 解析模板
	t, err := template.ParseFiles("demo_04_template/demo1.tmpl")
	if err != nil {
		fmt.Printf("parse template failed, err: %v\n", err)
		return
	}
	// 第三步: 渲染模板
	resultContent := "demo1 test"
	err = t.Execute(w, resultContent)
	if err != nil {
		fmt.Printf("render template failed, err: %v\n", err)
		return
	}
}
func main() {
	http.HandleFunc("/", demo1)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Printf("http server start failed, err: %v\n", err)
		return
	}
}
