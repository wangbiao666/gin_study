package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type User struct {
	gorm.Model
	Name   string
	Age    int64
	Active bool
}

func main() {
	db, err := gorm.Open("mysql", "root:123456@(10.10.10.101:33306)/db1?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// 自动创建表  自动迁移 （把结构体和数据表进行映射）
	db.AutoMigrate(&User{})

	//u1 := User{Name: "大狗", Age: 20, Active: true}
	//db.Create(&u1)
	//u2 := User{Name: "二狗", Age: 18, Active: false}
	//db.Create(&u2)

	// 更新：
	var user User
	//db.First(&user) // 查出来第一条数据做修改
	//user.Name = "大黄"
	//user.Age = 5
	//// 更新
	//db.Save(&user)
	//// 只更新一个字段
	//db.Model(&user).Update("name", "豆豆")

	m1 := map[string]interface{}{
		"name":   "biao",
		"age":    18,
		"active": true,
	}
	//db.Model(&user).Updates(m1)                // map里面列出的字段都会更新，修改所有的数据！
	//db.Model(&user).Select("age").Updates(m1) // 只更新age字段
	db.Model(&user).Omit("active").Updates(m1) // 排除active不更新，其他字段都更新

	// 更多更新操作参考：https://www.topgoer.cn/docs/gorm/gormgengxin
}
