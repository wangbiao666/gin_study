package main

import (
	"database/sql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"time"
)

type User struct {
	gorm.Model   // 内嵌 gorm.Model
	Name         string
	Age          sql.NullInt64 // 零值类型
	Birthday     *time.Time
	Email        string  `gorm:"type:varchar(100);unique_index"`
	Role         string  `gorm:"size:255"`        // 设置字段大小为255
	MemberNumber *string `gorm:"unique;not null"` // 设置会员号（member number）唯一并且不为空
	Num          int     `gorm:"AUTO_INCREMENT"`  // 设置 num 为自增类型
	Address      string  `gorm:"index:addr"`      // 给address字段创建名为addr的索引
	IgnoreMe     int     `gorm:"-"`               // 忽略本字段,这个列不会添加到表结构里
}

func main() {
	db, err := gorm.Open("mysql", "root:123456@(10.10.10.101:33306)/db1?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// 禁用复数设置：
	//	比如创建结构体User 对应的表，就叫user，不会叫users，去掉了s
	//db.SingularTable(true)

	db.AutoMigrate(&User{})
	// gorm 帮我们创建的表：
	//	如果结构体没有id，会帮我们创建一个id的列字段，
	// 	如果没有插入、修改、删除 的列字段，会帮我们添加列字段
	// 	created_at字段: 该字段的值将会是初次创建记录的时间。
	// 	updated_at字段，该字段的值将会是每次更新记录的时间。
	// 	deleted_at字段，调用Delete删除该记录时，将会设置deleted_at字段为当前时间，而不是直接将记录从数据库中删除。

	// 指定结构体对应的表名： 根据User 结构体创建出lalala的表
	db.Table("lalala").CreateTable(&User{})

	// SELECT * FROM deleted_users;
	db.Table("users").Find(&User{})

	// DELETE FROM deleted_users WHERE name = '111';
	db.Table("users").Where("name = ?", "111").Delete(&User{})
}
