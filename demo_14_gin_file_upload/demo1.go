package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"path"
)

func main() {
	r := gin.Default()

	r.LoadHTMLFiles("demo_14_gin_file_upload/index.html")
	// 跳转到index.html页面
	r.GET("/index", func(context *gin.Context) {
		context.HTML(http.StatusOK, "index.html", nil)
	})
	// 接收上传文件
	r.POST("/upload", func(context *gin.Context) {
		// 如果是接收多个文件，使用： context.MultipartForm().File("f1")
		f, err := context.FormFile("f1")
		if err != nil {
			context.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		} else {
			// 保存上传文件到当前目录
			dst := path.Join("demo_14_gin_file_upload/", f.Filename)
			context.SaveUploadedFile(f, dst)
			context.JSON(http.StatusOK, gin.H{
				"status": "OK",
			})
		}

	})

	r.Run(":9090")
}
