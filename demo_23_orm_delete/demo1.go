package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type User struct {
	gorm.Model
	Name   string
	Age    int64
	Active bool
}

func main() {
	db, err := gorm.Open("mysql", "root:123456@(10.10.10.101:33306)/db1?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// 自动创建表  自动迁移 （把结构体和数据表进行映射）
	db.AutoMigrate(&User{})

	//u1 := User{Name: "大狗", Age: 20, Active: true}
	//db.Create(&u1)
	//u2 := User{Name: "二狗", Age: 18, Active: false}
	//db.Create(&u2)

	// 删除：
	//var u = User{}
	//u.ID = 1
	//db.Delete(&u)
	//
	//db.Where("name = ?", "二狗").Delete(User{})
	//db.Delete(User{}, "age = ?", 18)
	//
	//var u1 []User
	//db.Where("name = ?", "二狗").Find(&u1)

	// 真正物理删除
	db.Unscoped().Where("name = ?", "biao").Delete(User{})
}
