package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

// 参数绑定
type UserInfo struct {
	// 如果是form格式，就是username参数,如果是请求的json格式，就用user参数
	Username string `form:"u" json:"user"`
	Password string `form:"p" json:"pwd"`
}

func main() {
	r := gin.Default()

	r.GET("/user", func(context *gin.Context) {
		// 之前所学的知识获取参数
		//	username := context.Query("username")
		//	password := context.Query("password")
		//	u := UserInfo{
		//		Username: username,
		//		Password: password,
		//	}
		//	fmt.Printf("%#v\n", u)
		//	context.JSON(http.StatusOK, gin.H{
		//		"message": "ok",
		//	})

		// 绑定：
		var u UserInfo
		err := context.ShouldBind(&u) // 绑定到结构体
		if err != nil {
			context.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		} else {
			fmt.Printf("%#v\n", u)
			context.JSON(http.StatusOK, gin.H{
				"status": "ok",
			})
		}
	})

	// 测试post请求方式
	r.POST("/form", func(context *gin.Context) {
		var u UserInfo
		//err := context.ShouldBind(&u)
		err := context.ShouldBindJSON(&u) // 绑定传入的json
		if err != nil {
			context.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		} else {
			fmt.Printf("%#v\n", u)
			context.JSON(http.StatusOK, gin.H{
				"status": "ok",
			})
		}
	})

	r.Run(":9090")
}
